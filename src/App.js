import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";

import Pokemonlist from "./screens/Pokemonlist";
import Navigation from "./components/Navigation";
import ContactUs from "./components/ContactUs";
import UseEffects from "./components/UseEffects";

function App() {
  return (
    <Router>
      <Navigation/>
    
      <Switch>
        <Route component={Pokemonlist} path="/Pokemonlist" />
        <Route component={ContactUs} path="/ContactUs"/>
        <Route component={UseEffects} path="/UseEffects"/>
      </Switch>
    </Router>
  );
}

export default App;

