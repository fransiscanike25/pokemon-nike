import React, { Component } from 'react';
import {
    Card, CardImg, CardText, CardBody,
    CardTitle,Button
  } from 'reactstrap'

export default function Pokemoncard ({Name,Attack,Specialpwr,img}) {
    return (
        <Card>
        <CardImg top width="100%" src={img} alt="ini gambar pokemon" />
        <CardBody>
          <CardTitle tag="h5">{Name}</CardTitle>
          <CardText>{Attack}</CardText>
          <CardText>{Specialpwr}</CardText>
          <Button>Catch Me</Button>
        </CardBody>
      </Card>
    )
}

