import React, { useState, useEffect } from "react";

export default function UseEffects (props) {
  const [name, setName] = useState("Mocil");
  
  useEffect(() => {
    document.title = name;
  });

  function handleNameChange(e) {
    setName(e.target.value);
  }

  return (
    <section>
      <input
        value={name}
        onChange={handleNameChange}
      />
    </section>
  );
  }