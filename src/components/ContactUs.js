import React, {useState} from 'react'
//belajar tentang hooks

export default function ContactUs() {
    const [name, setName] = useState("");
    const [message,setMessage] = useState ("");
    const [developers, setDevelopers] = useState ([
        {id: 1, name:'budi'},
        {id: 2, name: 'santo'},
    ]);

    const [newDev, setNewDev] = useState("");

    const handleSubmit = (event) => {
        event.preventDefault();
        setDevelopers ([
            ...developers,
            {
                id: developers.length + 1,
                name: newDev,
            },
        ]);
    };

    
    
    return (
        <div>
            <div className=''>Contact Us</div>

            <div>Namanya adalah : {name}</div>
            <div>Pesannya adalah : {message}</div>

            <div></div>
            <input 
            type= 'text'
            value={name}
            onChange={({target: {value}}) => setName(value)}>
            </input>
            <input
            type='text'
            value={message}
            onChange={({target: {value}}) => setMessage(value)}>
            </input>

            <div>Developers</div>
            {developers.map((developer) => (
                <div> {developer.name}</div>
            ))}

            
            <div className=''>Add Developer</div>
            <form onSubmit = {handleSubmit}>
                <input
                className=''
                type='text'
                value={newDev}
                onChange={({target: {value}}) => setNewDev (value)}
                />
                <button type='submit'>Tambah</button>
            </form>
            
        </div>
    )
}



