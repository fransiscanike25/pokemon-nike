import React from "react";
import {Row,Col} from "reactstrap";
import Pokemoncard from "../components/Pokemoncard";
import pikachu from "../images/pikachu.jpg";


export default function Pokemonlist (){
    const pokemoncards = [
        {
            Name:"Pikachu",
            Attack:"Medium",
            Specialpwr:"Speedlight",
            img: pikachu,
        },
        {
            Name:"Pikachu",
            Attack:"Medium",
            Specialpwr:"Speedlight",
            img:pikachu,
        },
        {
            Name:"Pikachu",
            Attack:"Medium",
            Specialpwr:"Speedlight",
            img:pikachu,
        },
        {
            Name:"Pikachu",
            Attack:"Medium",
            Specialpwr:"Speedlight",
            img:pikachu,
        },
        {
            Name:"Pikachu",
            Attack:"Medium",
            Specialpwr:"Speedlight",
            img:pikachu,
        },
        {
            Name:"Pikachu",
            Attack:"Medium",
            Specialpwr:"Speedlight",
            img:pikachu,
        },
        {
            Name:"Pikachu",
            Attack:"Medium",
            Specialpwr:"Speedlight",
            img:pikachu,
        },
        {
            Name:"Pikachu",
            Attack:"Medium",
            Specialpwr:"Speedlight",
            img:pikachu,
        },

    ];
    return(
        <div>
            <h3>The Pokemon</h3>
            <Row>
                {pokemoncards.map((pokemoncard)=>(
                    <Col md="4">
                        <Pokemoncard 
                        Name={pokemoncard.Name} 
                        Attack={pokemoncard.Attack} 
                        Specialpwr={pokemoncard.Specialpwr}
                        img={pokemoncard.img}/>
                    </Col>
                ))}
            </Row>
        </div>
    )
}
